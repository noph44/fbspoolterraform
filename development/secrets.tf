resource "aws_secretsmanager_secret" "development_fbsbowlpool_rds_secret" {
  name = "development-fbsbowlpool-rds"
}

resource "aws_secretsmanager_secret_version" "development_fbsbowlpool_rds_secret_version" {
  secret_id     = aws_secretsmanager_secret.development_fbsbowlpool_rds_secret.id
  secret_string = var.db-password
}

resource "aws_secretsmanager_secret" "development_fbsbowlpool_sg_api_key_secret" {
  name = "development-fbsbowlpool-sg-api-key"
}

resource "aws_secretsmanager_secret_version" "development_fbsbowlpool_sg_api_key_secret_version" {
  secret_id     = aws_secretsmanager_secret.development_fbsbowlpool_sg_api_key_secret.id
  secret_string = var.sg-api-key
}

resource "aws_secretsmanager_secret" "development_fbsbowlpool_admin_pw_secret" {
  name = "development-fbsbowlpool-admin-pw"
}

resource "aws_secretsmanager_secret_version" "development_fbsbowlpool_admin_pw_secret_version" {
  secret_id     = aws_secretsmanager_secret.development_fbsbowlpool_admin_pw_secret.id
  secret_string = var.admin-pw
}