resource "aws_ecs_task_definition" "development_fbsbowlpool_task_definition" {
  family                = "development-fbsbowlpool"
  container_definitions = file("task-definitions/fbsbowlpool.json")

  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 1024
  network_mode             = "awsvpc"
  task_role_arn            = aws_iam_role.development_ecs_task_role.arn
  execution_role_arn       = aws_iam_role.development_ecs_execution_role.arn
}

resource "aws_ecs_service" "development_fbsbowlpool_service" {
  name            = "fbsbowlpool"
  task_definition = aws_ecs_task_definition.development_fbsbowlpool_task_definition.arn
  cluster         = data.terraform_remote_state.development.outputs.fargate_cluster.id
  launch_type     = "FARGATE"
  desired_count   = 1

  load_balancer {
    target_group_arn = aws_lb_target_group.development_fbsbowlpool_tg.arn
    container_name   = "fbsbowlpool"
    container_port   = 80
  }

  network_configuration {
    assign_public_ip = true
    subnets = [
      data.terraform_remote_state.development.outputs.public_subnet_az1.id,
      data.terraform_remote_state.development.outputs.public_subnet_az2.id,
    ]
    security_groups = [aws_security_group.development_fbsbowlpool_sg.id]
  }

  lifecycle {
    ignore_changes = [task_definition]
  }
}