resource "aws_lb_listener_rule" "development_fbsbowlpool_alb_listener_rule" {
  listener_arn = data.terraform_remote_state.development.outputs.lb_listener.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.development_fbsbowlpool_tg.arn
  }

  condition {
    host_header {
      values = ["fbsbowlpool.dev.thenoph.com"]
    }
  }
}

resource "aws_lb_target_group" "development_fbsbowlpool_tg" {
  name        = "development-fbsbowlpool-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.terraform_remote_state.development.outputs.vpc.id
}