provider "aws" {}

data "aws_caller_identity" "aws_identity" {}

data "aws_availability_zones" "available_zones" {
  state = "available"
}

terraform {
  backend "remote" {
    organization = "ParadigmTechnologies"

    workspaces {
      name = "fbspoolterraform-development"
    }
  }
}

data "terraform_remote_state" "development" {
  backend = "remote"

  config = {
    organization = "ParadigmTechnologies"

    workspaces = {
      name = "aws-development"
    }
  }
}

