resource "aws_db_instance" "development_postgres_rds_db" {
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "11.12"
  instance_class         = "db.t2.micro"
  identifier             = "development-fbsbowlpool-rds"
  skip_final_snapshot    = true
  multi_az               = true
  name                   = "fbsbowlpool"
  username               = "fbsbowlpool"
  password               = var.db-password
  db_subnet_group_name   = aws_db_subnet_group.development_rds_subnets.name
  vpc_security_group_ids = [aws_security_group.development_fbsbowlpool_rds_sg.id]
}

resource "aws_db_subnet_group" "development_rds_subnets" {
  name = "development-rds-subnets"
  subnet_ids = [
    data.terraform_remote_state.development.outputs.private_subnet_az1.id,
    data.terraform_remote_state.development.outputs.private_subnet_az2.id,
  ]

  tags = {
    Name = "db private subnet group"
  }
}