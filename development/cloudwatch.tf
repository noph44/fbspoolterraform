resource "aws_cloudwatch_log_group" "development_fbsbowlpool_log_group" {
  name              = "/ecs/development-fbsbowlpool"
  retention_in_days = 30
  tags = {
    Application = "fbsbowlpool"
  }
}