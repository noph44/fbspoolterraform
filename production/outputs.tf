output "postgres_rds_db" {
  value     = aws_db_instance.production_postgres_rds_db
  sensitive = true
}

output "db_pw_secret" {
  value     = aws_secretsmanager_secret.production_fbsbowlpool_rds_secret
  sensitive = true
}

output "sg_api_key_secret" {
  value     = aws_secretsmanager_secret.production_fbsbowlpool_sg_api_key_secret
  sensitive = true
}

output "admin_pw_secret" {
  value     = aws_secretsmanager_secret.production_fbsbowlpool_admin_pw_secret
  sensitive = true
}