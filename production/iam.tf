resource "aws_iam_user" "fbspool_ecs_deploy_user" {
  name = "production-fbspool-ecs-deploy"
}

resource "aws_iam_user_policy" "fbspool_ecs_deploy_user_policy" {
  name   = "production-ecs-deployment-policy"
  user   = aws_iam_user.fbspool_ecs_deploy_user.name
  policy = data.aws_iam_policy_document.fbspool_ecs_deploy_user_policy_document.json
}

data "aws_iam_policy_document" "fbspool_ecs_deploy_user_policy_document" {
  statement {
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage",
      "ecs:DeregisterTaskDefinition",
      "ecs:RegisterTaskDefinition",
      "ecs:UpdateService",
      "iam:PassRole"
    ]
  }
}

resource "aws_iam_role" "production_ecs_task_role" {
  name               = "production-fbsbowlpool-ecs-task-role"
  assume_role_policy = data.aws_iam_policy_document.production_ecs_task_role_assume_role_policy_document.json
}

data "aws_iam_policy_document" "production_ecs_task_role_assume_role_policy_document" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "production_ecs_execution_role" {
  name               = "production-fbsbowlpool-ecs-execution-role"
  assume_role_policy = data.aws_iam_policy_document.production_ecs_execution_role_assume_role_policy_document.json
}

data "aws_iam_policy_document" "production_ecs_execution_role_assume_role_policy_document" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "production_ecs_task_role_policy" {
  name        = "production-ecs-scheduling-policy"
  path        = "/"
  description = "ECS Scheduling Policy"
  policy      = data.aws_iam_policy_document.production_fbsbowlpool_ecs_task_executionrole_policy_document.json
}

data "aws_iam_policy_document" "production_ecs_task_role_policy_document" {
  statement {
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:Describe*",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:RegisterTargets",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
  }
}

resource "aws_iam_policy" "production_fbsbowlpool_ecs_task_executionrole_policy" {
  name        = "production-fbsbowlpool-ecs-taskexecutionpolicy"
  path        = "/"
  description = "ECS TaskExecutionPolicy for fbsbowlpool containers"
  policy      = data.aws_iam_policy_document.production_fbsbowlpool_ecs_task_executionrole_policy_document.json
}

data "aws_iam_policy_document" "production_fbsbowlpool_ecs_task_executionrole_policy_document" {
  statement {
    effect  = "Allow"
    actions = ["secretsmanager:GetSecretValue"]
    resources = [
      aws_secretsmanager_secret.production_fbsbowlpool_rds_secret.arn,
      aws_secretsmanager_secret.production_fbsbowlpool_sg_api_key_secret.arn,
      aws_secretsmanager_secret.production_fbsbowlpool_admin_pw_secret.arn,
    ]
  }
}

resource "aws_iam_role_policy_attachment" "production_ecs_role_policy_attachment" {
  role       = aws_iam_role.production_ecs_task_role.name
  policy_arn = aws_iam_policy.production_ecs_task_role_policy.arn
}

resource "aws_iam_role_policy_attachment" "production_ecs_execution_role_policy_attachement" {
  role       = aws_iam_role.production_ecs_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "production_ecs_task_execution_role_policy_attachment" {
  role       = aws_iam_role.production_ecs_execution_role.name
  policy_arn = aws_iam_policy.production_ecs_task_role_policy.arn
}

resource "aws_iam_role_policy_attachment" "production_fbsbowlpool_ecs_task_execution_role_policy_attachment" {
  role       = aws_iam_role.production_ecs_execution_role.name
  policy_arn = aws_iam_policy.production_fbsbowlpool_ecs_task_executionrole_policy.arn
}