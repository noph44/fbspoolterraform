data "aws_route53_zone" "primary_zone" {
  zone_id = "ZB0FKTLM94EBG"
}

resource "aws_route53_record" "production_fbsbowlpool_cname_record" {
  zone_id = data.aws_route53_zone.primary_zone.zone_id
  name    = "fbsbowlpool"
  type    = "CNAME"
  records = ["fbsbowlpool.prd.thenoph.com"]
  ttl     = "30"
}