resource "aws_cloudwatch_log_group" "production_fbsbowlpool_log_group" {
  name              = "/ecs/production-fbsbowlpool"
  retention_in_days = 30
  tags = {
    Application = "fbsbowlpool"
  }
}