resource "aws_lb_listener_rule" "production_fbsbowlpool_alb_listener_rule" {
  listener_arn = data.terraform_remote_state.production.outputs.lb_listener.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.production_fbsbowlpool_tg.arn
  }

  condition {
    host_header {
      values = ["fbsbowlpool.prd.thenoph.com"]
    }
  }
}

resource "aws_lb_listener_rule" "production_fbsbowlpool_cname_alb_listener_rule" {
  listener_arn = data.terraform_remote_state.production.outputs.lb_listener.arn
  priority     = 2

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.production_fbsbowlpool_tg.arn
  }

  condition {
    host_header {
      values = ["fbsbowlpool.thenoph.com"]
    }
  }
}

resource "aws_lb_target_group" "production_fbsbowlpool_tg" {
  name        = "production-fbsbowlpool-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.terraform_remote_state.production.outputs.vpc.id
}