data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_key_pair" "fbsbowlpool" {
  key_name = "update-db"
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t3.micro"
  associate_public_ip_address = true
  key_name                    = data.aws_key_pair.fbsbowlpool.key_name
  subnet_id                   = data.terraform_remote_state.production.outputs.public_subnet_az1.id
  security_groups             = [aws_security_group.production_fbsbowlpool_rds_update_sg.id]
}