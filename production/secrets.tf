resource "aws_secretsmanager_secret" "production_fbsbowlpool_rds_secret" {
  name = "production-fbsbowlpool-rds"
}

resource "aws_secretsmanager_secret_version" "production_fbsbowlpool_rds_secret_version" {
  secret_id     = aws_secretsmanager_secret.production_fbsbowlpool_rds_secret.id
  secret_string = var.db_password
}

resource "aws_secretsmanager_secret" "production_fbsbowlpool_sg_api_key_secret" {
  name = "production-fbsbowlpool-sg-api-key"
}

resource "aws_secretsmanager_secret_version" "production_fbsbowlpool_sg_api_key_secret_version" {
  secret_id     = aws_secretsmanager_secret.production_fbsbowlpool_sg_api_key_secret.id
  secret_string = var.sg_api_key
}

resource "aws_secretsmanager_secret" "production_fbsbowlpool_admin_pw_secret" {
  name = "production-fbsbowlpool-admin-pw"
}

resource "aws_secretsmanager_secret_version" "production_fbsbowlpool_admin_pw_secret_version" {
  secret_id     = aws_secretsmanager_secret.production_fbsbowlpool_admin_pw_secret.id
  secret_string = var.admin_pw
}