resource "aws_db_instance" "production_postgres_rds_db" {
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "11.16"
  instance_class         = "db.t2.micro"
  identifier             = "production-fbsbowlpool-rds"
  skip_final_snapshot    = true
  multi_az               = true
  name                   = "fbsbowlpool"
  username               = "fbsbowlpool"
  password               = var.db_password
  db_subnet_group_name   = aws_db_subnet_group.production_private_subnets.name
  vpc_security_group_ids = [aws_security_group.production_fbsbowlpool_rds_sg.id]
}

resource "aws_db_subnet_group" "production_private_subnets" {
  name = "production-private-subnets"
  subnet_ids = [
    data.terraform_remote_state.production.outputs.private_subnet_az1.id,
    data.terraform_remote_state.production.outputs.private_subnet_az2.id,
  ]

  tags = {
    Name = "db private subnet group"
  }
}