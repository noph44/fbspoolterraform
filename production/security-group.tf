resource "aws_security_group" "production_fbsbowlpool_sg" {
  name        = "production-fbsbowlpool-sg"
  description = "Allow traffic from the load balancer"
  vpc_id      = data.terraform_remote_state.production.outputs.vpc.id

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    security_groups = [data.terraform_remote_state.production.outputs.lb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "production_fbsbowlpool_rds_sg" {
  name        = "production-fbsbowlpool-rds-sg"
  description = "Allow traffic to the db from the ecs tasks"
  vpc_id      = data.terraform_remote_state.production.outputs.vpc.id

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    security_groups = [
      aws_security_group.production_fbsbowlpool_sg.id,
      aws_security_group.production_fbsbowlpool_rds_update_sg.id
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "production_fbsbowlpool_rds_update_sg" {
  name        = "production-fbsbowlpool-rds-update-sg"
  description = "Allow traffic to the db from the ec2 instance for modifying the database"
  vpc_id      = data.terraform_remote_state.production.outputs.vpc.id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["104.55.24.190/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}