provider "aws" {}

data "aws_caller_identity" "aws-identity" {}

data "aws_availability_zones" "available-zones" {
  state = "available"
}

data "terraform_remote_state" "shared" {
  backend = "remote"

  config = {
    organization = "ParadigmTechnologies"

    workspaces = {
      name = "aws-shared"
    }
  }
}